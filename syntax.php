<?php
/**
 * Plugin Inline Code : embed inline code 
 *
 * Syntax: `your code`
 * 
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     CrazyCat <crazycat@c-p-f.org>
 */

class syntax_plugin_inlinecode extends DokuWiki_Syntax_Plugin {

	function getType(){
		return 'formatting';
	}

	function getAllowedTypes() {
		return array('formatting', 'substition', 'disabled');
	}

	function getPType(){
		return 'normal';
	}

	function getSort(){
		return 999;
	}

	function connectTo($mode) {
		$this->Lexer->addEntryPattern('`', $mode, 'plugin_inlinecode');
	}

	function postConnect() {
		$this->Lexer->addExitPattern('`', 'plugin_inlinecode');
	}

	function handle($match, $state, $pos, Doku_Handler $handler){
		$data['match'] = $match;
		switch ($state) {
			case DOKU_LEXER_ENTER : 
				return array($state, $data);
				break;
			case DOKU_LEXER_EXIT :
				return array($state, $data);
				break;
			case DOKU_LEXER_UNMATCHED:
				$handler->_addCall('cdata', array($match), $pos);
				return false;
				break;
		}
		return array();
	}

	function render($mode, Doku_Renderer $renderer, $data) {
		list($state, $data) = $data;
		if($mode == 'xhtml'){
			switch($state) {
				case DOKU_LEXER_ENTER:
				$renderer->doc .= '<span class="inline_code">';
				return true;
				break;
			case DOKU_LEXER_EXIT:
				$renderer->doc .= '</span>';
				return true;
				break;
			}
		}
		return false;
	}
}